### Create a BIOS- and EFI-bootable usb drive with grub

+ Partition a usb drive, as follows:
  - DOS/MBR partition table
  - one partition for entire disk
  - Type code: ef
  - Format the partition with FAT:

        mkfs.vfat -n ESP /dev/sdX1

+ Mount the partition:

        mkdir /tmp/usb
        mount /dev/sdX1 /tmp/usb

+ Install grub for EFI and PC:

        grub-install --target=x86_64-efi --efi-directory=/tmp/usb --boot-directory=/tmp/usb/boot
        grub-install --target=i386-pc --boot-directory=/tmp/usb/boot /dev/sdX

+ Create the EFI boot directory:

        cd /tmp/usb/EFI/
        mkdir boot
        cp -p grub/grubx64.efi boot/bootx64.efi
        rm -r grub

+ Download the netboot and hd-media kernel/initrd combinations to the drive:

        cd /tmp/usb/boot
        mkdir net hd
        cd net
        wget http://ftp.debian.org/debian/dists/stretch/main/installer-amd64/current/images/netboot/debian-installer/amd64/linux
        wget http://ftp.debian.org/debian/dists/stretch/main/installer-amd64/current/images/netboot/debian-installer/amd64/initrd.gz
        cd ../hd
        wget http://ftp.debian.org/debian/dists/stretch/main/installer-amd64/current/images/hd-media/vmlinuz
        wget http://ftp.debian.org/debian/dists/stretch/main/installer-amd64/current/images/hd-media/initrd.gz

+ Deploy the `grub.cfg` template and update script:

        cd "$repo_root"
        rsync -rtP boot/ /tmp/usb/boot/

+ Deploy the desired ISOs and corresponding grub-loop-boot config snippets:

        cd /tmp/usb/boot
        rsync -tP *.iso isos
        rsync -tP *.cfg grub/grub.d/
